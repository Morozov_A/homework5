from flask import Flask, render_template
from flask_migrate import Migrate

from views.products import product_app
from models.database import db


app = Flask(__name__)
app.config.from_object('config.DevelopmentConfig')

db.init_app(app)
migrate = Migrate(app, db)

app.register_blueprint(product_app, url_prefix="/products")


@app.route("/")
def hello_index():
    return render_template("index.html")


if __name__ == "__main__":
    app.run(
        host="0.0.0.0",
        port=5000,
        debug=True,
    )
