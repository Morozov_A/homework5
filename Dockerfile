#Заходим в терминал
#В директории /24.Docker.ver-con
#docker build . -t My_tea
#run -p 5000:5000 My_tea

FROM python:3.9-buster

WORKDIR /var/app

COPY my-shop/requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY My_tea .

EXPOSE 5000

CMD python main.py
